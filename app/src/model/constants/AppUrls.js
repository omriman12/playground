const AppUrls = {
    Users:{
        Root: '/Users'
    },
    Home:{
        Root: '/Home'
    },
    About:{
        Root: '/About'
    },
    Contact:{
        Root: '/Contact'
    },
    Assets:{
        Root: '/Assets'
    },
}

export default AppUrls;